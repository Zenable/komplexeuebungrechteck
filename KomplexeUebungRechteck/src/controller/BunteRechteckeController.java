package controller;

import java.util.LinkedList;
import java.util.List;
import java.util.Random;

import model.MySQLDatabase;
import model.Rechteck;
import view.Eingabemaske;

public class BunteRechteckeController {
	
	private List<Rechteck> rechtecke = new LinkedList<Rechteck>();
	private MySQLDatabase database;
	
	public BunteRechteckeController() {
		this.rechtecke = new LinkedList<Rechteck>();
		this.database = new MySQLDatabase();
		rechtecke = this.database.getAlleRechtecke();
	}
	
	public void add(Rechteck rechteck){
		rechtecke.add(rechteck);
		this.database.rechteckEintragen(rechteck);
	}
	
	public void reset() {
		rechtecke.clear();
	}
	
	public List<Rechteck> getRechtecke() {
		return this.rechtecke;
	}
	
	public void generiereZufallsRechteck(int anzahl) {
		Rechteck r = new Rechteck();
		
		for(int i = 0; i < anzahl; i++) {
			this.add(r.generiereZufallsRechteck());
		}
	}

	@Override
	public String toString() {
		return "BunteReckteckeController [rechtecke=" + rechtecke + "]";
	}

	public void rechteckHinzufuegen() {
		new Eingabemaske(this);
	}
	
	

}
