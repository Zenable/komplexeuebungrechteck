package view;

import java.awt.Color;
import java.awt.Graphics;
import javax.swing.JPanel;

import controller.BunteRechteckeController;
import model.Rechteck;

public class Zeichenflaeche extends JPanel {

	private BunteRechteckeController bunterecht;
	
	public Zeichenflaeche(BunteRechteckeController bunterecht) {
		this.bunterecht = bunterecht;
	}
	
	@Override
	public void paintComponent(Graphics g) {
		g.setColor(Color.BLACK);
		g.drawRect(0, 0, 50, 50);
		
		for (int i = 0; i < bunterecht.getRechtecke().size(); i++) {
		g.drawRect(bunterecht.getRechtecke().get(i).getX(), bunterecht.getRechtecke().get(i).getY(), bunterecht.getRechtecke().get(i).getBreite(), bunterecht.getRechtecke().get(i).getHoehe());
		}
	}
}
