package view;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.BunteRechteckeController;
import model.Rechteck;

import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import java.awt.Insets;
import java.awt.GridLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.LayoutStyle.ComponentPlacement;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Eingabemaske extends JFrame {

	private JPanel contentPane;
	private JTextField textFieldX;
	private JTextField textFieldY;
	private JTextField textFieldHoehe;
	private JTextField textFieldBreite;
	private BunteRechteckeController brc;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					BunteRechteckeController brc = new BunteRechteckeController();
					Eingabemaske frame = new Eingabemaske(brc);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @param brc 
	 */
	public Eingabemaske(BunteRechteckeController brc) {
		this.brc = brc;
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 324, 227);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new GridLayout(2, 1, 0, 0));
		
		JPanel panel = new JPanel();
		contentPane.add(panel);
		panel.setLayout(new GridLayout(0, 2, 0, 0));
		
		JLabel lblX = new JLabel("X");
		panel.add(lblX);
		
		textFieldX = new JTextField();
		panel.add(textFieldX);
		textFieldX.setColumns(10);
		
		JLabel lblY = new JLabel("Y");
		panel.add(lblY);
		
		textFieldY = new JTextField();
		panel.add(textFieldY);
		textFieldY.setColumns(10);
		
		JLabel lblHoehe = new JLabel("H\u00F6he");
		panel.add(lblHoehe);
		
		textFieldHoehe = new JTextField();
		panel.add(textFieldHoehe);
		textFieldHoehe.setColumns(10);
		
		JLabel lblBreite = new JLabel("Breite");
		panel.add(lblBreite);
		
		textFieldBreite = new JTextField();
		panel.add(textFieldBreite);
		textFieldBreite.setColumns(10);
		
		JPanel panelBtn = new JPanel();
		contentPane.add(panelBtn);
		
		JButton btnSpeichern = new JButton("Speichern");
		btnSpeichern.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				rechteckSpeichern();
			}
		});
		GroupLayout gl_panelBtn = new GroupLayout(panelBtn);
		gl_panelBtn.setHorizontalGroup(
			gl_panelBtn.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_panelBtn.createSequentialGroup()
					.addGap(77)
					.addComponent(btnSpeichern, GroupLayout.DEFAULT_SIZE, 150, Short.MAX_VALUE)
					.addGap(71))
		);
		gl_panelBtn.setVerticalGroup(
			gl_panelBtn.createParallelGroup(Alignment.LEADING)
				.addGroup(Alignment.TRAILING, gl_panelBtn.createSequentialGroup()
					.addGap(35)
					.addComponent(btnSpeichern, GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE)
					.addGap(31))
		);
		panelBtn.setLayout(gl_panelBtn);
		
		setVisible(true);
	}
	
	public void rechteckSpeichern() {
		int x = Integer.parseInt(textFieldX.getText());
		int y = Integer.parseInt(textFieldY.getText());
		int hoehe = Integer.parseInt(textFieldHoehe.getText());
		int breite = Integer.parseInt(textFieldBreite.getText());
		this.brc.add(new Rechteck(x, y, breite, hoehe));
	} 
}
