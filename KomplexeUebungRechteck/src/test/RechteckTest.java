package test;

import model.Rechteck;
import controller.BunteRechteckeController;

public class RechteckTest {

	public static void main(String[] args) {
		Rechteck recht0 = new Rechteck();
		recht0.setBreite(30);
		recht0.setHoehe(40);
		recht0.setX(10);
		recht0.setY(10);
		
		//if (recht0.toString().equals("Rechteck [x=10, y=10, breite=30, hoehe=40]"))
			//System.out.println(recht0.toString());
		
		Rechteck recht1 = new Rechteck();
		recht1.setBreite(100);
		recht1.setHoehe(20);
		recht1.setX(25);
		recht1.setY(25);
		
		Rechteck recht2 = new Rechteck();
		recht2.setBreite(200);
		recht2.setHoehe(100);
		recht2.setX(260);
		recht2.setY(10);
		
		Rechteck recht3 = new Rechteck();
		recht3.setBreite(300);
		recht3.setHoehe(25);
		recht3.setX(5);
		recht3.setY(500);
		
		Rechteck recht4 = new Rechteck();
		recht4.setBreite(100);
		recht4.setHoehe(100);
		recht4.setX(100);
		recht4.setY(100);
		
		Rechteck recht5 = new Rechteck(200, 200, 200, 200);
		
		Rechteck recht6 = new Rechteck(800, 400, 20, 20);
		
		Rechteck recht7 = new Rechteck(800, 450, 20, 20);
		
		Rechteck recht8 = new Rechteck(850, 400, 20, 20);
		
		Rechteck recht9 = new Rechteck(855, 455, 25, 25);
		
		BunteRechteckeController bunterecht = new BunteRechteckeController();
		bunterecht.add(recht0);
		bunterecht.add(recht1);
		bunterecht.add(recht2);
		bunterecht.add(recht3);
		bunterecht.add(recht4);
		bunterecht.add(recht5);
		bunterecht.add(recht6);
		bunterecht.add(recht7);
		bunterecht.add(recht8);
		bunterecht.add(recht9);
		
		System.out.println(bunterecht.toString());

		//Prüfen ob Rechteckerstellung mit negativen Breiten und Höhen möglich ist
		Rechteck eck10 = new Rechteck(-4,-5,-50,-200);
		System.out.println(eck10); //Rechteck [x=-4, y=-5, breite=50, hoehe=200]
		Rechteck eck11 = new Rechteck();
		eck11.setX(-10);
		eck11.setY(-10);
		eck11.setBreite(-200);
		eck11.setHoehe(-100);
		System.out.println("eck11" + eck11);//Rechteck [x=-10, y=-10, breite=200, hoehe=100]
		
		Rechteck eck12 = Rechteck.generiereZufallsRechteck();
		System.out.println("eck12" + eck12);
		
		rechteckeTesten();
	}
	
	public static void rechteckeTesten() {
		int mengeRechtecke = 50000;
		
		Rechteck[] rechtecke = new Rechteck[mengeRechtecke];
		Rechteck pruefrechteck = new Rechteck(0, 0, 1200, 1000);
		
		for(int i = 0; i < mengeRechtecke; i++) {
			rechtecke[i] = Rechteck.generiereZufallsRechteck();
			if(!pruefrechteck.enthaelt(rechtecke[i])) {
				System.out.println("Fehler. Fehler! " + rechtecke[i].toString() + " ist au�erhalb der Grenzen. T�TEN SCHNELL!!");
				return;
			}
		}
		
		System.out.println("Alles paletti!");
	
	}

}