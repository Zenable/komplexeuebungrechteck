package model;

import java.awt.Toolkit;
import java.util.Random;

import controller.BunteRechteckeController;

public class Rechteck {
	
	private Punkt p = new Punkt();
	private int breite;
	private int hoehe;
	private static final int BILDSCHIRMHOEHE = (int) Toolkit.getDefaultToolkit().getScreenSize().getHeight();
	private static final int BILDSCHIRMBREITE = (int)Toolkit.getDefaultToolkit().getScreenSize().getWidth();
	
	public Rechteck(){
		this.p.setX(0);
		this.setY(0);
		this.breite = 0;
		this.hoehe = 0;
	}
	
	public Rechteck(int x, int y, int breite, int hoehe) {
		this.setX(x);
		this.setY(y);
		this.setBreite(breite);
		this.setHoehe(hoehe);
	}

	public int getX() {
		return p.getX();
	}

	public void setX(int x) {
		this.p.setX(x);
	}

	public int getY() {
		return p.getY();
	}

	public void setY(int y) {
		this.p.setY(y);
	}

	public int getBreite() {
		return breite;
	}

	public void setBreite(int breite) {
		this.breite = Math.abs(breite);
	}

	public int getHoehe() {
		return hoehe;
	}

	public void setHoehe(int hoehe) {
		this.hoehe = Math.abs(hoehe);
	}
	
	public boolean enthaelt(int x, int y) {
		if ((x <= this.getX() + this.getBreite()) && (x >= this.getX()) &&
                (y <= this.getY() + this.getHoehe()) && (y >= this.getY()))
			return true;
		return false;
	}
	
	public boolean enthaelt(Punkt p) {
		return this.enthaelt(p.getX(), p.getY());
	}

	public boolean enthaelt(Rechteck rechteck){
	    return this.enthaelt(rechteck.getX(), rechteck.getY()) && 
	    		this.enthaelt(rechteck.getX()+rechteck.getBreite(), rechteck.getY()+rechteck.getHoehe());
    }

    public static Rechteck generiereZufallsRechteck(){
    	 Random random = new Random();
	     int x = random.nextInt(BILDSCHIRMBREITE);
	     int y = random.nextInt(BILDSCHIRMHOEHE);
	     int hoehe = random.nextInt((BILDSCHIRMHOEHE - y) + 1);
	     int breite = random.nextInt((BILDSCHIRMBREITE - x) + 1);
	     return new Rechteck(x, y, breite, hoehe);
    }

	@Override
	public String toString() {
		return "Rechteck [x=" + p.getX() + ", y=" + p.getY() + ", breite=" + breite +
                ", hoehe=" + hoehe + "]";
	}
	
}
