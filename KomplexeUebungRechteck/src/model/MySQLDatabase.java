package model;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;

public class MySQLDatabase {
	private String driver = "com.mysql.jdbc.Driver";
	private String url = "jdbc:mysql://localhost/rechtecke?";
	private String user = "root";
	private String password = "";
	
	public void rechteckEintragen(Rechteck r) {
		try {
			//JDBC Treiber laden
			Class.forName(driver);
			//Verbindung aufbauen
			Connection con = DriverManager.getConnection(url, user, password);
			
			String sql = "INSERT INTO T_rechtecke(x, y, breite, hoehe) VALUES(?, ?, ?, ?);";
			PreparedStatement ps = con.prepareStatement(sql);
			ps.setInt(1, r.getX());
			ps.setInt(2, r.getY());
			ps.setInt(3, r.getBreite());
			ps.setInt(4, r.getHoehe());
			ps.executeUpdate();
			//Verbindung schlie�en
			con.close();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<Rechteck> getAlleRechtecke(){
		List rechtecke = new LinkedList<Rechteck>();
		try {
			//JDBC Treiber laden
			Class.forName(driver);
			//Verbindung aufbauen
			Connection con = DriverManager.getConnection(url, user, password);
			String sql = "SELECT * FROM T_rechtecke;";
			Statement s = con.createStatement();
			ResultSet rs = s.executeQuery(sql);
			//Auswertung und Umwandlung in Rechtecke
			while(rs.next()) {
				int x = rs.getInt("x");
				int y = rs.getInt("y");
				int breite = rs.getInt("breite");
				int hoehe = rs.getInt("hoehe");
				rechtecke.add(new Rechteck(x, y, breite, hoehe));
			}
			//Verbindung schlie�en
			con.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return rechtecke;
	}
}